from functools import partial

from decimal import Decimal

from PySide6.QtWidgets import(
    QMainWindow,QApplication,QWidget,QLabel,QPushButton,QGridLayout,
    QLCDNumber
)



botones=[
    ['C'],
    ['7','8','9','/'],
    ['4','5','6','x'],
    ['1','2','3','-'],
    ['.','0','=','+']
]


class Calculadora(QLCDNumber):
    def __init__(self):
        super().__init__()
        self.Flat
        self.setDigitCount(20)
        self.setMinimumHeight(80)


class Finestra(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Calculadora")
        self.calculcadora = Calculadora()
        self.calculcadora.adjustSize
        self.respuesta = QLabel("0")
        self.taulesEscas=QGridLayout()
    #    self.taulesEscas.addWidget(self.calculcadora,0,1,1,4)
        self.taulesEscas.addWidget(self.calculcadora,0,0,1,0)


        self.numero1 = ""
        self.operation = ""
        self.vista = ""
        self.opracion=""

        
        for i, keys in enumerate(botones):
            for n,keys in enumerate(keys):
                posicion = botones[i][n]
                button = QPushButton(str(botones[i][n]))
                button.setStyleSheet("height:50px;font-size:25px;")
                button.setObjectName(str(posicion))
                button.clicked.connect(partial(self.calcular,posicion))
                if posicion == 'C':
                    self.taulesEscas.addWidget(button,1,0,1,4)
                else:    
                    self.taulesEscas.addWidget(button,i+1,n)

        
        contenedor =QWidget()
        contenedor.setLayout(self.taulesEscas)
        self.setCentralWidget(contenedor)

        self.resize(480,360)


      

    def calcular(self,dato):
        operators = ['/', 'x', '-',  '+']

        print(dato)
        if dato == 'C':
            self.limpiar()

        elif not dato in operators and dato != '=':
            if dato == '.' and self.vista != "" and  '.' not in self.vista:
                self.vista +=dato
                self.calculcadora.display(self.vista)
            elif dato.isnumeric():
                self.vista +=dato 
                self.calculcadora.display(self.vista)

        elif dato in operators and self.vista != "":
            if self.numero1 == "":
                self.operation = dato
                self.numero1 = self.vista
                self.vista = ""
                self.calculcadora.display(0)
        elif dato == '=':
            self.mostrarResultado()
        else:
            print("que pasa")
    



        
    def mostrarResultado(self):
        print("Formula: " + self.numero1 + "  " +  self.operation + " " + self.vista )

        print(self.numero1 != "" )
        print(self.operation != "" )
        print(self.vista != "")
        resultado = '0'

        if self.numero1 != "" and self.operation != "" and self.vista != "":
            print("Empieza la operacion")
            if self.operation == '+':
                print("Suma")
                resultado = str(Decimal(self.numero1) + Decimal(self.vista))
            elif self.operation == '-':
                resultado = str(Decimal(self.numero1) - Decimal(self.vista))
                print("resta")
            elif self.operation == 'x':
                resultado = str(Decimal(self.numero1) * Decimal(self.vista))
                print("multi")

            elif self.operation == '/':
                try:
                    resultado = str(Decimal(self.numero1) / Decimal(self.vista))
                except ZeroDivisionError:
                    resultado = 'Error'
                print("division")
            if len(resultado) < 20:
                print(len(resultado) )
                self.calculcadora.display(resultado.lstrip())
            else:
                print(len(resultado) )
                self.calculcadora.display('Error')
            self.vista=""
            self.numero1=""
            self.operation=""

    def limpiar(self):
        print('limpiar')
        self.vista=""
        self.numero1=""
        self.operation=""
        self.calculcadora.display(0)

if __name__ == "__main__":
    app = QApplication()
    finestreta = Finestra()
    finestreta
    finestreta.show()
    app.exec()
